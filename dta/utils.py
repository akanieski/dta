import datetime

def unix_stamp(dt):
    return (dt - datetime.datetime(1970, 1, 1)).total_seconds() * 1000