from django.test import TransactionTestCase
import dta.models as dta_models
import json


class SystemTestCase(TransactionTestCase):
    fixtures = ['basic.json', 'basic_user.json']

    def test_authenticate(self):
        payload = {
            "username": "admin",
            "password": "testpass"
        }
        response = self.client.post(
            '/api/auth/', content_type='application/json', data=json.dumps(payload))

        data = json.loads(response.content)

        self.assertNotEqual(
            data.get('token'), None, "Token authentication must be 'successful'")

    def test_use_expired_token(self):
        response = self.client.get(
            '/api/userprofile/active', **{'HTTP_API_KEY': 'expired-token-here'})

        self.assertEqual(
            response.status_code, 401, "Use of expired tokens must be disallowed")

    def test_create_user(self):
        payload = {
            "user": {
                "password_confirm": "test",
                "password": "test",
                "username": "testadmin",
                "first_name": "Timmy",
                "last_name": "Tester",
                "email": "test.dta@xoxy.net"
            }
        }
        response = self.client.post(
            '/api/userprofile/', content_type='application/json', data=json.dumps(payload))

        data = json.loads(response.content)

        self.assertEqual(
            data.get('success'), True, "Create user profile must be 'successful'")

    def test_update_user(self):
        payload = {
            "id": 2,
            "user": {
                "password_confirm": "changeme",
                "password": "changeme",
                "username": "user",
                "first_name": "Andrew",
                "last_name": "Kanieski",
                "email": "a@a.com"
            }
        }
        response = self.client.put('/api/userprofile/2/', content_type='application/json', data=json.dumps(payload),
                                   **{'HTTP_API_KEY': 'good-token-non-admin'})

        data = json.loads(response.content)

        self.assertEqual(
            data.get('success'), True, "Update user profile must be 'successful'")

    def test_update_user_username_conflict(self):
        payload = {
            "id": 2,
            "user": {
                "password_confirm": "changeme",
                "password": "changeme",
                "username": "admin",
                "first_name": "Andrew",
                "last_name": "Kanieski",
                "email": "a@a.com"
            }
        }
        response = self.client.put('/api/userprofile/2/', content_type='application/json', data=json.dumps(payload),
                                   **{'HTTP_API_KEY': 'good-token-non-admin'})

        data = json.loads(response.content)

        self.assertEqual(data.get(
            'success'), False, "Update user profile with conflicting username must be disallowed")
        self.assertEqual(response.status_code, 409,
                         "Update user profile with conflicting username must be disallowed")

    def test_get_active_profile(self):
        response = self.client.get('/api/userprofile/active',
                                   **{'HTTP_API_KEY': '23de1a6e-13b2-11e5-b98c-c82a1402a930'})

        data = json.loads(response.content)

        self.assertNotEqual(data.get('user'), None,
                            "Must contain data")
        self.assertNotEqual(data.get('domains'), None,
                            "Must contain a complete set of data and not be a \'simple\' view")

    def test_try_get_other_profile(self):
        response = self.client.get('/api/userprofile/1/',
                                   **{'HTTP_API_KEY': 'good-token-non-admin'})

        self.assertEqual(
            response.status_code, 200, "Must return valid status code")

        data = json.loads(response.content)

        self.assertEqual(data.get(
            'domains'), None, "Must not allow complete retrieval of another users profile")

    def test_create_domain(self):
        response = self.client.get('/api/userprofile/active',
                                   **{'HTTP_API_KEY': '23de1a6e-13b2-11e5-b98c-c82a1402a930'})

        data = json.loads(response.content)

        self.assertNotEqual(data.get('user'), None, "Must contain data")
