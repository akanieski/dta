# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dta', '0002_userprofile'),
    ]

    operations = [
        migrations.AlterField(
            model_name='authtoken',
            name='expiration',
            field=models.DateTimeField(),
        ),
    ]
