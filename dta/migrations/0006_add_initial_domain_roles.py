# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def add_initial_roles(apps, schema_editor):
    AppRole = apps.get_model('dta', 'AppRole')
    roles = [
        {
            "name": "Administrator",
            "description": "Domain Administrator has full access to modify and edit elements in the domain"
        },
        {
            "name": "Standard User",
            "description": "Standard user has basic access to the domain"
        }
    ]
    for role in roles:
        if AppRole.objects.filter(name=role['name']).count() == 0:
            AppRole.objects.create(
                name=role['name'],
                description=role['description']
            )


class Migration(migrations.Migration):

    dependencies = [
        ('dta', '0005_appdomain_approle_userappdomain'),
    ]

    operations = [
        migrations.RunPython(add_initial_roles),
    ]
