import uuid
import datetime
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

from dta import settings


class AppDomain(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)

    def as_dict_full(self):
        return {
            "name": self.name
        }


class AuthToken(models.Model):
    token = models.CharField(max_length=255, null=False)
    expiration = models.DateTimeField(null=False)
    user = models.ForeignKey(User, null=False)

    def renew_token(self):
        self.expiration = datetime.datetime.utcnow()
        self.expiration = timezone.make_aware(self.expiration + datetime.timedelta(days=settings.TOKEN_EXPIRATION),
                                              timezone.utc)


class UserProfile(models.Model):
    user = models.ForeignKey(User, null=False)

    def as_simple_dict(self):
        return {
            "id": self.id,
            "user": {
                "email": self.user.email,
                "username": self.user.username,
                "last_name": self.user.last_name,
                "first_name": self.user.first_name
            }
        }

    def as_full_dict(self):
        domains = {}
        for uad in self.userappdomain_set.all():
            if uad.appdomain.id not in domains:
                domains[uad.appdomain.id] = uad.appdomain.as_dict_full()
            if "roles" not in domains[uad.appdomain.id]:
                domains[uad.appdomain.id]["roles"] = []
            domains[uad.appdomain.id]["roles"].add(uad.role.as_dict())

        domain_list = []

        for d in domains:
            domain_list.add({
                "name": domains[d]["name"],
                "roles": domains[d]["roles"]
            })

        return {
            "id": self.id,
            "domains": domain_list,
            "user": {
                "id": self.user.id,
                "email": self.user.email,
                "username": self.user.username,
                "last_name": self.user.last_name,
                "first_name": self.user.first_name
            }
        }

    def generate_token(self):
        expiration = datetime.datetime.utcnow()
        expiration = timezone.make_aware(
            expiration + datetime.timedelta(days=settings.TOKEN_EXPIRATION), timezone.utc)
        return AuthToken.objects.create(
            token=str(uuid.uuid1()),
            expiration=expiration,
            user=self.user
        )


class AppRole(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)
    description = models.CharField(max_length=1000)

    def as_dict_full(self):
        return {
            "name": self.name,
            "description": self.description
        }


class UserAppDomain(models.Model):
    userprofile = models.ForeignKey(UserProfile)
    appdomain = models.ForeignKey(AppDomain)
    role = models.ForeignKey(AppRole)
