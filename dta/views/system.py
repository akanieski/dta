import json
import datetime
from django.db import transaction
from django.utils import timezone
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User, UserManager
from django.contrib.auth import authenticate

from dta.models import UserProfile, AuthToken, AppDomain, UserAppDomain, AppRole


def token_required(func):
    def inner(request, *args, **kwargs):
        if request.method == 'OPTIONS':
            return func(request, *args, **kwargs)
        auth_header = request.META.get('HTTP_API_KEY', None)
        if auth_header is not None:
            try:
                request.token = AuthToken.objects.get(token=auth_header)

                if not request.token.expiration or timezone.make_aware(datetime.datetime.utcnow(), timezone.utc) > request.token.expiration:
                    return json_response({
                        "error": "Token expired"
                    }, status=401)

                request.token.renew_token()
                request.token.save()

                return func(request, *args, **kwargs)
            except AuthToken.DoesNotExist:
                return json_response({
                    'error': 'Token not found'
                }, status=401)
        return json_response({
            'error': 'Invalid Header'
        }, status=401)

    return inner


def json_response(response_dict, status=200):
    response = HttpResponse(
        json.dumps(response_dict), content_type="application/json", status=status)
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Headers'] = 'Content-Type, Authorization'
    return response

# Domain Management


@csrf_exempt
@token_required
def create_domain(request):
    if request.method == 'POST':
        try:
            package = json.loads(request.body)
        except ValueError:
            package['success'] = False
            package['error'] = 'Invalid json request'
            return json_response(package, 400)

        data = package.get('data')

        if not data:
            package['success'] = False
            package['error'] = 'Invalid json request'
            return json_response(package, 400)

        if not data.get('userprofile_id'):
            package['success'] = False
            package['error'] = 'User profile id is missing'
            return json_response(package, 400)

        if not data.get('name'):
            package['success'] = False
            package['error'] = 'Name of domain is missing'
            return json_response(package, 400)

        domain = AppDomain.objects.create(
            name=data.get('name', '')
        )
        admin_role = AppRole.objects.get(name='Administrator')
        userdomainrole = UserAppDomain.objects.create(
            userprofile_id=data.get('userprofile_id', ''),
            appdomain_id=domain.id,
            role_id=admin_role.id
        )

        package['success'] = True
        return json_response(package, 200)
    else:
        return json_response({"error": "Unsupported verb"}, 400)

#########


@csrf_exempt
@token_required
def userprofile_update(request, userprofile_id):
    data = json.loads(request.body)
    try:
        profile = UserProfile.objects.get(pk=userprofile_id)
    except UserProfile.DoesNotExist:
        return json_response({"error": "User profile not found"}, 404)

    for field in []:
        if field in data:
            setattr(profile, field, data[field])
    profile.save()

    try:
        user = User.objects.get(pk=profile.user.id)
    except User.DoesNotExist:
        return json_response({"error": "User profile not found"}, 404)

    # request made by token's owner or by superuser
    if user.id == request.token.user.id or request.token.user.is_superuser:

        for field in ['username', 'first_name', 'last_name', 'email']:
            if field in data['user']:
                setattr(user, field, data['user'][field])

        if User.objects.filter(username=data['user']['username']).exclude(pk=user.id).count() > 0:
            return json_response({
                'error': 'Username already in use',
                'data': data,
                'success': False
            }, 409)

        user.save()

        # password
        if {'password', 'password_confirm'} <= set(data['user'].keys()):
            if data['user']['password'] == data['user']['password_confirm']:
                user.set_password(data['user']['password'])
        else:
            return json_response({
                'error': 'Password does not match confirmation password',
                'data': data
            }, 400)

        return json_response({
            "success": True,
            "data": UserProfile.objects.get(pk=userprofile_id).as_full_dict()
        }, 200)
    else:
        return json_response({"error": "Not authorized", "success": False}, 401)


@csrf_exempt
@transaction.atomic
def userprofile_create(request):
    data = json.loads(request.body)
    user_data = data.get('user')
    try:
        user = User.objects.create_user(
            username=user_data.get('username'),
            email=user_data.get('email'),
            password=user_data.get('password')
        )
    except ValueError:
        data['error'] = 'Invalid request'
        return json_response(data, 400)

    profile = UserProfile.objects.create(
        user=user
    )
    profile_dict = profile.as_full_dict()

    profile_dict['token'] = profile.generate_token().token

    return json_response({
        "success": True,
        "data": profile_dict
    }, 200)


@token_required
@csrf_exempt
def userprofile_delete(request, userprofile_id):
    profile = UserProfile.objects.get(pk=userprofile_id)
    user = User.objects.get(pk=profile.user.id)

    # request made by token's owner or by superuser
    if user.id == request.user.id or request.token.user.is_superuser:
        profile.delete()
        user.delete()
        return json_response({"success": True}, 200)

    else:
        return json_response({"success": False}, 401)


@token_required
@csrf_exempt
def userprofile_get(request, userprofile_id):
    try:
        if userprofile_id is None:
            userprofile_id = request.token.user.id
        profile = UserProfile.objects.get(pk=userprofile_id)

        # request made by token's owner or by superuser
        if profile.user.id == request.user.id or request.token.user.is_superuser:
            return json_response(profile.as_full_dict(), 200)
        else:
            return json_response(profile.as_simple_dict(), 200)

    except UserProfile.DoesNotExist:
        return json_response({"error": "User profile not found"}, 404)


@csrf_exempt
def userprofile(request, userprofile_id=None):
    if request.method == 'GET':
        return userprofile_get(request, userprofile_id)

    elif request.method == 'PUT':
        return userprofile_update(request, userprofile_id)

    elif request.method == 'POST':
        return userprofile_create(request)

    elif request.method == 'DELETE':
        return userprofile_delete(request, userprofile_id)


@csrf_exempt
def auth(request):
    data = json.loads(request.body)

    if not data.get('username'):
        return json_response({"errors": {"username": "Username is required"}})

    if not data.get('password'):
        return json_response({"errors": {"password": "Password is required"}})

    user = authenticate(
        username=data.get('username'), password=data.get('password'))

    if not user:
        return json_response({"errors": {"password": "Failed to authenticate"}})

    try:
        userprofile = UserProfile.objects.get(user_id=user.id)
    except UserProfile.DoesNotExist:
        userprofile = UserProfile.objects.create(user=user)

    token = userprofile.generate_token()

    return json_response({"token": token.token})
